<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name_provider');
            $table->string('email')->unique()->nullable();
            $table->string('name');
            $table->string('phone');
            $table->string('name_business')->nullable();
            $table->string('url')->nullable();
            $table->string('phone_secondary')->nullable();
            $table->boolean('status')->defoult(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
