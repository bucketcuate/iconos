<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('date');
            $table->time('duration');
            $table->time('start_time');
            $table->time('final_time');
            $table->text('coments');
            $table->float('subtotal');
            $table->float('total');
            $table->float('received');
            $table->float('turned');
            $table->boolean('status')->default(1);
            $table->foreignId('client_id')->nullable()->constrained();
            $table->foreignId('payment__type_id')->constrained();
            $table->foreignId('user_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
