<?php

namespace Database\Factories;

use App\Models\Service_Appointment;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceAppointmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service_Appointment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
