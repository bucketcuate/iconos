<?php

namespace Database\Factories;

use App\Models\Material_Service;
use Illuminate\Database\Eloquent\Factories\Factory;

class MaterialServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Material_Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
