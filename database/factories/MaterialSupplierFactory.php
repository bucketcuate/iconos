<?php

namespace Database\Factories;

use App\Models\Material_Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

class MaterialSupplierFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Material_Supplier::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
