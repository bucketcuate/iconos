<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset("css/app.css")}}"/>
</head>
<body class="bg-contraste bg-cover bg-no-repeat">
    <div class="container bg-red-200">
        <div class="h-40 w-40 bg-white bg-opacity-50">
            <span>hola desde blade</span>
        </div>
    </div>
</body>
</html>
