<?php

namespace App\Http\Controllers;

use App\Models\Material_Service;
use Illuminate\Http\Request;

class MaterialServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Material_Service  $material_Service
     * @return \Illuminate\Http\Response
     */
    public function show(Material_Service $material_Service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Material_Service  $material_Service
     * @return \Illuminate\Http\Response
     */
    public function edit(Material_Service $material_Service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Material_Service  $material_Service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Material_Service $material_Service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Material_Service  $material_Service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material_Service $material_Service)
    {
        //
    }
}
