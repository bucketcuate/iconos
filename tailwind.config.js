module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container:{ center:true,},
    extend: {
      backgroundImage: theme => ({
        'contraste': "url('/assets/img/background.jpg')"
       })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
